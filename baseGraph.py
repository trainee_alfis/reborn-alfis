import config
from neuthink.graph import basics as bg
from neuthink.graph import neo4j_graph as pg

graph = pg.PersistentGraph(config.graph_connection, graph_segment="segment_graph")      # подключаемся к графу

isEmpty = lambda string: string if string != "пусто" else ""                            # проверяем значение на пустоту, так как некоторые слова имеют значение ""

def parseNode(fileName: str):
    """функция открывает файл fileName, считывает его и создаёт узлы графа. Файл должен быть в одном каталоге с baseGraph.py
    
    Arguments:
        fileName {str} -- название файла
    """
    with open(fileName, "r") as wordsFile:                                               
        nodes = [node.rstrip().split(" ") for node in wordsFile]
        [bg.Node(graph, {node[0]: node[1], node[2]: isEmpty(node[3]), node[4]: isEmpty(node[5])}) for node in nodes]