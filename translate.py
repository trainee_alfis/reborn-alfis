import config
from neuthink.graph import basics as bg
from neuthink.graph import neo4j_graph as pg
from baseGraph import parseNode
from typing import List

graph = pg.PersistentGraph(config.graph_connection, graph_segment="segment_graph")      # подключаемся к графу

def interface(message: str) -> str:
    """выводит message и возвращает ответ
    
    Arguments:
        message {str} -- сообщение, которое нужно вывести

    Returns:
        str -- ответ пользователя на сообщение
    """
    return input(message).lower()

def reverseLanguage(language: str) -> str:
    """определяем язык, на который нужно перевести текст
    
    Arguments:
        language {str} -- язык, на котором написан исходный текст

    Returns:
        str -- язык, на который нужно перевести
    """
    return "русский" if language == "английский" else "английский"

def onlyAlphaSplit(string: str) -> List[str]:
    """оставляет в строке только буквы, возвращает список слов
    
    Arguments:
        string {str} -- строка, которую нужно обработать
    
    Returns:
        List[str] -- список слов
    """
    return "".join([letter for letter in string if letter.isalpha() or letter == " "]).split(" ")

def translate(wordsList: List[str]) -> str:
    """находит слово в графе, переводит его, если оно есть. Иначе оставляет это слово не переведённым
    
    Arguments:
        wordsList {List[str]} -- список слов, которые нужно перевести
    
    Returns:
        List[str] -- строку с переведёнными словами
    """
    return " ".join([graph.Match({language: word})[0][rlanguage] if len(graph.Match({language: word})) > 0 else word for word in wordsList])

if len(graph.Match({})) == 0:                                                           # если граф пустой, он заполняется узлами из файла words.txt
    parseNode("words.txt")

string = interface("введите ваше предложение, которое вы хотите перевести: \n")         

language = interface("На каком языке написан ваш текст?\n")                           

rlanguage = reverseLanguage(language)                                                  

print("Ваш перевод:\n" + translate(onlyAlphaSplit(string)))                             # выводим переведённый текст